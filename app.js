const express = require('express')
const app = express()
const port = 80

const ytdl = require("ytdl-core");
const TeleBot = require('telebot');
const bot = new TeleBot({token: "5922070786:AAF51mDQKQ8Z0F9uosCi9CG4yMcsX9BTpRI"})
const youTubeValidateRegEx = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?[\w\?‌​=]*)?/m;


console.log("Run app...");
startBot();
startServer();
function startBot() {
    console.log("Start bot listening...");
    bot.on('text', async msg => {
        console.log("On get message...");
        const id = msg.from.id;
        const text = msg.text;

        if(text === "/start") {
            return bot.sendMessage(id, "Paste link on Youtube video...");
        }

        const result = await getVideosDataByLink(text, (format) => {
            return `[<a href="${format.url}">${format.qualityLabel}</a>]`;
        })
        return bot.sendMessage(id, result.error || result.data.join('\n'), {parseMode: "HTML"});
    });
    bot.connect();
}

function startServer() {
    app.get('/showvideosbylink/', async (req, res) => {
        const result = await getVideosDataByLink(req.query["link"], (format) => {
            return `[<a href="${format.url}">${format.qualityLabel}</a>]`;
        })

        if(result.error) {
            res.send(result.error)
        }
        else {
            res.send(`
                <!DOCTYPE html>
                <html>
                <body>${result.data.join('<br />')}</body>
                </html>
            `)
        }
    })

    app.get('/gay', (req, res) => {
        res.sendFile(__dirname + '/gay.html');
    });

    app.listen(port, () => {
        console.log(`App listening on port ${port}`)
    });
}

/**
 *
 * @param link
 * @param processor
 * @returns {error|result}
 */
async function getVideosDataByLink(link, processor = null) {
    if(link.match(youTubeValidateRegEx)) {
        const info = await ytdl.getInfo(link);
        const res = []
        for (const format of info.formats) {
            if(format.hasAudio && format.mimeType.indexOf("video/mp4;") !== -1) {
                res.push(processor != null ? processor(format) : format)
            }
        }
        return {data: res};
    } else {
        return {error: "Youtube link required."};
    }
}